

import { BehaviorSubject } from 'rxjs';
import jwt from "jsonwebtoken";
import history from "../helpers/history"
import { config } from '../config/config'

const currentUserSubject = new BehaviorSubject(localStorage.getItem('token'));

export const authenticationService = {
    login,
    logout,
    isAuthenticated,
    register,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue () { return currentUserSubject.value }
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${config.host}:${config.port}/api/user/login`, requestOptions)
    .then(response => response.json())
        .then(user => {
                localStorage.setItem('token', JSON.stringify(user));
                currentUserSubject.next(user);

                return user;
        }).catch(err => {
            console.log(err);
            throw err;
        });
}

function register(username, password){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${config.host}:${config.port}/api/user/register`, requestOptions)
    .then(response => { 
        if(response.ok){
            return true;
        }
    })
    .catch(err =>{
        console.log(err);
        throw err;
    });
}

function logout() {
    localStorage.removeItem('token');
    currentUserSubject.next(null);
}

function isAuthenticated() {

    let result = false;

    const token = JSON.parse(localStorage.getItem('token'));
    console.log(token);
    if(token){

        const { exp } = jwt.decode(token.access_token);
        const expirationTime = exp * 1000;
        console.log(expirationTime)
        if(Date.now() >= expirationTime)
        {
            localStorage.clear();
            history.push("/");
        }
        else
        {
            result = true;
        }
    }

    return result;
}
