
import { config } from "../config/config";

export const characterService = {
    getCharacterInformation
};

function getCharacterInformation() {

    let token = JSON.parse(localStorage.getItem('token'));
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization' :  `${token.type} ${token.access_token}` }
    };

    return fetch(`${config.host}:${config.port}/api/character`, requestOptions)
    .then(response => response.json());
}