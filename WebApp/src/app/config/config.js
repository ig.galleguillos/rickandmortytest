

export const config = {
    host: process.env.API__HOST || "http://localhost",
    port: process.env.API__PORT && parseInt(process.env.API__PORT) || 3000
}