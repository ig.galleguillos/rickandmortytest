

import React from 'react';
import { Container, Row, Col } from "react-bootstrap";
import { authenticationService } from '../services/AuthService';
import history from "../helpers/history"

export default class LoginComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '' 
        }

        this.login = this.login.bind(this);
        this.redirectToSignUp = this.redirectToSignUp.bind(this);
    }

    
    login(){
            authenticationService.login(this.state.username, this.state.password).then(() =>{
                this.props.history.push('/home')
            }).catch(err => {
                alert("Invalid Credentials");
            });
        
    }

    handleUsername = event => {
        this.setState({username: event.target.value});
    };

    handlePassword = event => {
        this.setState({password: event.target.value});
    }

    redirectToSignUp(){

        this.props.history.push('/register');
    }

    render() {
        return (
            <Container>
                <Row style={{padding:"100px"}}>
                    <Col></Col>
                    <Col>
                    <form className="form-signin">
                        <h1 className="h3 mb-3 font-weight-normal">Please log in</h1>
                        <label htmlFor="inputEmail" className="sr-only">Email address</label>
                        <input type="text" id="inputEmail" className="form-control" placeholder="Username" required autoFocus value={this.state.username} onChange={this.handleUsername}/>
                        <label htmlFor="inputPassword" className="sr-only">Password</label>
                        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required value={this.state.password} onChange={this.handlePassword}/>
                        <label><a className="btn" onClick={this.redirectToSignUp}>Sign Up</a></label>
                        <div style={{padding:"10px"}}>
                            <a onClick={this.login} className="btn btn-lg btn-primary btn-block" >Sign in</a>
                        </div>
                    </form>
                    </Col>
                    <Col></Col>
                </Row>
            </Container>
        );
    }
}