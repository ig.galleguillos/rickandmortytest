

import React from 'react';
import { authenticationService } from '../services/AuthService';
import { characterService } from '../services/CharacterService';

export default class HomeComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: authenticationService.currentUserValue,
            characters: []
        };

        this.logout = this.logout.bind(this);
    }

    componentDidMount(){

        characterService.getCharacterInformation().then(data =>{
            console.log(data);
            this.setState({ characters : data });
        })
    }

    loadCharacterInformation(){

        
    }

    logout(){
        authenticationService.logout();
        this.props.history.push('/')
    }

    render() {
        return (
            <div>
            <div>
                <nav className="navbar navbar-expand navbar-dark bg-dark">
                    <div className="navbar-nav">
                        <a onClick={this.logout} className="nav-item nav-link">Logout</a>
                    </div>
                </nav>
                    
            </div>
            <div className="container p-4">
                <h1>Rick And Morty Characters Information</h1>
                <table className="table table-borderer">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Specie</th>
                            <th>Gender</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.characters.map((data, i) => {
                                console.log(data)
                                return <tr key={i}>
                                    <td>{data.name}</td>
                                    <td>{data.status}</td>
                                    <td>{data.species}</td>
                                    <td>{data.gender}</td>
                                    <td><img style={{ width: "100px", height: "100px"}} src={data.imageUrl}/></td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            </div>
            </div>
        );
    }
}
