

import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import LoginComponent from './LoginComponent';
import HomeComponent from './HomeComponent';
import history from "../helpers/history"
import SignUpComponent from './SignUpComponent';


export default class AppComponent extends React.Component {

    constructor(props){
        super(props);
    }

    componentDidMount() {
        
    }

    render(){
        return (
            <Router history={history}>
                <Route exact path="/" component={LoginComponent} />
                <Route path="/register" component={SignUpComponent}/>
                <PrivateRoute exact path="/home" component={HomeComponent} />
            </Router> 
            
        )
    }
}
