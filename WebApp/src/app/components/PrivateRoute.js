

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { authenticationService } from '../services/AuthService'

const PrivateRoute = ({component: Component, ...rest}) => {

    return (
        <Route {...rest} render={
          props => ( authenticationService.isAuthenticated() ? <Component {...rest} {...props} /> : <Redirect to="/"/>
          )} />
      )
};

export default PrivateRoute;