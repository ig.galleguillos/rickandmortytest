
import React from 'react';
import { Container, Row, Col } from "react-bootstrap";
import { authenticationService } from '../services/AuthService';

export default class SignUpComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '' 
        }

        this.signUp = this.signUp.bind(this);
    }

    signUp(){

        authenticationService.register(this.state.username, this.state.password).then(() => {
            alert("User successfully registered");
            this.props.history.push('/');
        });
        
    }

    handleUsername = event => {
        this.setState({username: event.target.value});
    };

    handlePassword = event => {
        this.setState({password: event.target.value});
    }

    render() {
        return (
            <Container>
                <Row style={{padding:"100px"}}>
                    <Col></Col>
                    <Col>
                    <form className="form-signin">
                        <h1 className="h3 mb-3 font-weight-normal">Please Sign Up</h1>
                        <label htmlFor="inputEmail" className="sr-only">Email address</label>
                        <input type="text" id="inputEmail" className="form-control" placeholder="Username" required autoFocus value={this.state.username} onChange={this.handleUsername}/>
                        <label htmlFor="inputPassword" className="sr-only">Password</label>
                        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required value={this.state.password} onChange={this.handlePassword}/>
                        <div style={{padding:"10px"}}>
                            <a onClick={this.signUp} className="btn btn-lg btn-primary btn-block" >Sign Up</a>
                        </div>
                    </form>
                    </Col>
                    <Col></Col>
                </Row>
            </Container>
        );
    }
}