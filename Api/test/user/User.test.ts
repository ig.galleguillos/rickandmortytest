
import "reflect-metadata";

import { Container } from "inversify";
import { TYPES } from "../../src/infrastructure/configuration/Types";
import IUserRepository from "../../src/domain/interfaces/IUserRepository";
import UserRepository from "../../src/infrastructure/repositories/UserRepository";
import ICreateUser from "../../src/application/use_cases/user_create/ICreateUser";
import CreateUser from "../../src/application/use_cases/user_create/CreateUser";
import User from "../../src/domain/entities/User";
import CreateUserResponse from "../../src/application/use_cases/user_create/CreateUserResponse";
import CreateUserRequest from "../../src/application/use_cases/user_create/CreateUserRequest";
import IHashing from "../../src/domain/interfaces/IHashing";
import HashingService from "../../src/infrastructure/hashing/HashingService";
import AuthenticateUserRequest from "../../src/application/use_cases/authenticate_user/AuthenticateUserRequest";
import AuthenticateUserResponse from "../../src/application/use_cases/authenticate_user/AuthenticateUserResponse";
import AuthenticateUser from "../../src/application/use_cases/authenticate_user/AuthenticateUser";
import IJwtTokenManager from "../../src/domain/interfaces/IJwtTokenManager";
import JwtTokenManager from "../../src/infrastructure/jwt/JwtTokenManager";

describe("User test", () => {
    
    let userRepository: IUserRepository;
    let createUser : ICreateUser;
    let hashingService : IHashing;
    let jwtTokenManager: IJwtTokenManager

    const container = new Container();

    container.bind<IUserRepository>(TYPES.IUserRepository).to(UserRepository);
    container.bind<IHashing>(TYPES.IHashing).to(HashingService);
    container.bind<ICreateUser>(TYPES.ICreateUser).to(CreateUser);
    container.bind<IJwtTokenManager>(TYPES.IJwtTokenManager).to(JwtTokenManager);

    beforeEach(() =>{
        userRepository = container.get<IUserRepository>(TYPES.IUserRepository);
        createUser = container.get<ICreateUser>(TYPES.ICreateUser);
        hashingService = container.get<IHashing>(TYPES.IHashing);
        jwtTokenManager = container.get<IJwtTokenManager>(TYPES.IJwtTokenManager);
    });

    test("should return a created user in repository", async () => {

        let user:User = new User("id", "username", "password");
        let createUserResponse : CreateUserResponse = new CreateUserResponse("id", "username");
        let createUserRequest : CreateUserRequest = new CreateUserRequest("username", "password");

        let userRepository : UserRepository = new UserRepository();
        let createUser : CreateUser = new CreateUser(hashingService, userRepository);

        userRepository.AddUser = await jest.fn((user: User) => Promise.resolve(user));
        createUser.CreateUser = await jest.fn((createUserRequest : CreateUserRequest) => Promise.resolve(createUserResponse));

        userRepository.AddUser(user).then(data => {
            expect(data).toBe(user);
        })

        createUser.CreateUser(createUserRequest).then(data => {
            expect(data).toBe(createUserResponse);
        });
    });

    test("should return token when user is logged", async () => {

        let user:User = new User("id", "username", "password");
        let authenticateUserRequest : AuthenticateUserRequest = new AuthenticateUserRequest("id", "password");
        let authenthicateUserResponse : AuthenticateUserResponse = new AuthenticateUserResponse("token", "Bearer");

        let userRepository : UserRepository = new UserRepository();
        let authUser : AuthenticateUser = new AuthenticateUser(userRepository, hashingService, jwtTokenManager);

        userRepository.GetUser = await jest.fn((userInput: string) => Promise.resolve(user));
        authUser.AuthenticateUser = await jest.fn((authenticateUserRequest: AuthenticateUserRequest) => Promise.resolve(authenthicateUserResponse));

        userRepository.GetUser("username").then(data => {
            expect(data).toBe(user);
        });

        authUser.AuthenticateUser(authenticateUserRequest).then(data => {
            expect(data).toBe(authenthicateUserResponse);
        });
    });
})