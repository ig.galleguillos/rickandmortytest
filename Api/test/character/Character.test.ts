import "reflect-metadata";

import Character from "../../src/domain/entities/Character";
import CharacterService from "../../src/application/use_cases/characters/CharacterService";


describe("Character test", () => {

    test("should return character information about rick and morty's characters", async () => {

        let characters : Character[] = [{
            "name": "Rick Sanchez",
            "status": "Alive",
            "species": "Human",
            "gender": "Male",
            "imageUrl": "https://rickandmortyapi.com/api/character/avatar/1.jpeg"
        },
        {
            "name": "Morty Smith",
            "status": "Alive",
            "species": "Human",
            "gender": "Male",
            "imageUrl": "https://rickandmortyapi.com/api/character/avatar/2.jpeg"
        }];

        let characterService: CharacterService = new CharacterService();

        characterService.GetRickAndMortyCharacterInformation = await jest.fn(() => Promise.resolve(characters));

        characterService.GetRickAndMortyCharacterInformation().then(data =>{
            expect(data).toBe(characters);
        })
    });
});