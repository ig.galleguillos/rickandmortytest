
export const RedisConfig = {

    redis:{
        host: process.env.REDIS__HOST || "0.0.0.0",
        port: process.env.REDIS__PORT && parseInt(process.env.REDIS__PORT) || 6379
    }
}