import IHashing from "../../domain/interfaces/IHashing";
import { resolve } from "path";
import { stringify } from "querystring";

import bcrypt from "bcrypt";
import { injectable } from "inversify";

@injectable()
export default class HashingService implements IHashing{

    async HashPassword(password: string): Promise<string> {

        const saltRounds = 10;
        let result = "";

        await bcrypt.genSalt(saltRounds).then(async (salts) => {
            await bcrypt.hash(password, salts).then((hash) => {
                result = hash;
            })
        }).catch((err) =>{
            throw err;
        });

        return result;
    }

    async CompareHashedPassword(passwordFromRequest: string, passwordFromDb: string): Promise<boolean>{
        let result: boolean = false;

        await bcrypt.compare(passwordFromRequest, passwordFromDb).then(data => {
            result = data;
        });

        return result;
    }
}