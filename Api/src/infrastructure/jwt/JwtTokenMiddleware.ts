

import * as Koa from "koa";
import jwt from "jsonwebtoken";
import { JwtSecret } from "./JwtSecret";

export default async function JwtTokenMiddleware(ctx: Koa.Context, next: () => Promise<any>)
{
    try
    {
        let token = ctx.get("Authorization");
        token = token.replace("Bearer ", "");
        jwt.verify(token, JwtSecret.secret);
        await next();
    }
    catch(err)
    {
        ctx.response.status = 401;
    }
}