import IJwtTokenManager from "../../domain/interfaces/IJwtTokenManager";
import User from "../../domain/entities/User";
import { injectable } from "inversify";
import jwt from "jsonwebtoken";
import { JwtSecret } from "./JwtSecret";


@injectable()
export default class JwtTokenManager implements IJwtTokenManager {

    async GenerateToken(user: User): Promise<string>{

        return jwt.sign({userId: user.id, username: user.username }, JwtSecret.secret, {
            expiresIn : 120
        })
    }
}