
export const TYPES = {
    IUserRepository : Symbol.for("IUserRepository"),
    IHashing : Symbol.for("IHashing"),
    ICreateUser : Symbol.for("ICreateUser"),
    IAuthenticateUser : Symbol.for("IAuthenticateUser"),
    IJwtTokenManager : Symbol.for("IJwtTokenManager"),
    ICharacterService : Symbol.for("ICharacterService")
}