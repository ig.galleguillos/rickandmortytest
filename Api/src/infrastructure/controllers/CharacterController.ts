
import { controller, interfaces, httpPost, response, request, httpGet } from "inversify-koa-utils";
import { injectable, inject } from "inversify";
import * as Koa from "koa";
import { TYPES } from "../configuration/Types";
import CharacterService from "../../application/use_cases/characters/CharacterService";
import ICharacterService from "../../application/use_cases/characters/ICharacterService";
import JwtTokenHandler  from "../jwt/JwtTokenMiddleware";

@controller("/api/character")
@injectable()
export class CharacterController implements interfaces.Controller {
    
    private readonly characterService: ICharacterService;

    constructor(@inject(TYPES.ICharacterService) characterService: CharacterService)
    {
        this.characterService = characterService;
    }

    @httpGet("/", JwtTokenHandler)
    public async GetCharacterInformation(@request() req: Koa.Request, @response() res: Koa.Response){

        await this.characterService.GetRickAndMortyCharacterInformation().then(result => {
            res.body = result;
            res.status = 200;
        }).catch(err => {
            console.log(err);
            res.status = 400;
        });

        
    }
}
