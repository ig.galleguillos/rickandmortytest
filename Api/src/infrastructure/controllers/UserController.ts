
import { controller, interfaces, httpPost, response, request } from "inversify-koa-utils";
import { injectable, inject } from "inversify";
import * as Koa from "koa";
import ICreateUser from "../../application/use_cases/user_create/ICreateUser";
import { TYPES } from "../configuration/Types";
import CreateUserRequest from "../../application/use_cases/user_create/CreateUserRequest";
import IAuthenticateUser from "../../application/use_cases/authenticate_user/IAuthenticateUser";
import AuthenticateUserRequest from "../../application/use_cases/authenticate_user/AuthenticateUserRequest";

@controller("/api/user")
@injectable()
export class UserController implements interfaces.Controller{

    private readonly createUserUseCase : ICreateUser;
    private readonly authenticateUser : IAuthenticateUser;

    constructor(@inject(TYPES.ICreateUser) createUserUseCase: ICreateUser,
                @inject(TYPES.IAuthenticateUser) authenticateUser : IAuthenticateUser){

        this.createUserUseCase = createUserUseCase;
        this.authenticateUser = authenticateUser;
    }

    @httpPost("/register")
    public async Register(@request() req: Koa.Request, @response() res: Koa.Response){

        let user = new CreateUserRequest(req.body.username, req.body.password);

        await this.createUserUseCase.CreateUser(user).then(() => {
            res.status = 201;
            res.body = user;
        }).catch((err) =>{
            console.log(err);
            res.status = 400;
            res.body = err.message;
        });
    }

    @httpPost("/login")
    public async Login(@request() req: Koa.Request, @response() res: Koa.Response){
        
        let authenticateRequest = new AuthenticateUserRequest(req.body.username, req.body.password);

        await this.authenticateUser.AuthenticateUser(authenticateRequest).then((result) => {
            res.body = result;
            res.status = 200;
        }).catch((err) => {
            console.log(err);
            res.status = 400;
            res.body = err.message;
        });
    }
}