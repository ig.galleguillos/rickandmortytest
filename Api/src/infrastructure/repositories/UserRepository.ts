
import IUserRepository from "../../domain/interfaces/IUserRepository";
import User from "../../domain/entities/User";
import { RedisConfig } from "../redis/RedisConfig";
import { RedisClient, createClient } from "redis";
import { v4 as uuidv4} from "uuid";
import { injectable } from "inversify";
import { promisify } from "util";

@injectable()
export default class UserRepository implements IUserRepository{

    private redisClient : RedisClient;

    private getAsync : any;
    private setAsync : any;
    
    constructor(){
        this.redisClient = createClient(RedisConfig.redis);

        // this.redisClient.on("connect", () =>{
        //     console.log(`connected to redis on  ${RedisConfig.redis.host}:${RedisConfig.redis.port}`)
        // });

        this.getAsync = promisify(this.redisClient.hget).bind(this.redisClient);
        this.setAsync = promisify(this.redisClient.hset).bind(this.redisClient);
    }

    async AddUser(user:User): Promise<User> {
        
        user.id = uuidv4();

        let userExist = await this.getAsync("users", user.username);

        if(userExist == null){
            await this.setAsync("users", user.username, JSON.stringify(user));
        }else{
            throw Error("user already exists");
        }
    
        return user;
    }

    async GetUser(username: string): Promise<User> {
        return await this.getAsync("users", username).then((result:any) => {
            let user = JSON.parse(result);
            
            if(user){
                return new User(user.id, user.username, user.password);
            }else{
                throw Error("user not found");
            }
            
        });
    }

}