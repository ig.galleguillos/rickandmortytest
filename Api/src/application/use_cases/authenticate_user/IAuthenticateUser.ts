import AuthenticateUserRequest from "./AuthenticateUserRequest";
import AuthenticateUserResponse from "./AuthenticateUserResponse";


export default interface IAuthenticateUser {

    AuthenticateUser(authenticateUserRequest: AuthenticateUserRequest) : Promise<AuthenticateUserResponse>;
}