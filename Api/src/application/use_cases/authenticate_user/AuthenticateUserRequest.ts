
export default class AuthenticateUserRequest {

    constructor(
        public username :string, 
        public password: string
    )
    {}
}