

export default class AuthenticateUserResponse {

    constructor
    (
        public access_token: string,
        public type: string
    ){}
}