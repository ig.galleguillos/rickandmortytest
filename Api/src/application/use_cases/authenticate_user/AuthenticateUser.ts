import IAuthenticateUser from "./IAuthenticateUser";
import AuthenticateUserRequest from "./AuthenticateUserRequest";
import AuthenticateUserResponse from "./AuthenticateUserResponse";
import IUserRepository from "../../../domain/interfaces/IUserRepository";
import { inject, injectable } from "inversify";
import { TYPES } from "../../../infrastructure/configuration/Types";
import IHashing from "../../../domain/interfaces/IHashing";
import IJwtTokenManager from "../../../domain/interfaces/IJwtTokenManager";

@injectable()
export default class AuthenticateUser implements IAuthenticateUser {

    private userRepository: IUserRepository;
    private hashingPassword: IHashing;
    private jwtTokenManager: IJwtTokenManager;
    
    constructor(@inject(TYPES.IUserRepository) userRepository: IUserRepository,
                @inject(TYPES.IHashing) hashingPassword : IHashing,
                @inject(TYPES.IJwtTokenManager) jwtTokenManager : IJwtTokenManager)
    {
        this.userRepository = userRepository;
        this.hashingPassword = hashingPassword;
        this.jwtTokenManager = jwtTokenManager;
    }

    async AuthenticateUser(authenticateUserRequest: AuthenticateUserRequest): Promise<AuthenticateUserResponse> {
        
        let token:string = "";
        let user = await this.userRepository.GetUser(authenticateUserRequest.username);

        let compareResult = await this.hashingPassword.CompareHashedPassword(authenticateUserRequest.password, user.password);

        if(compareResult)
        {
            await this.jwtTokenManager.GenerateToken(user).then(data => {
                token = data;
            }).catch((err) => { 
                console.log(err);
                throw Error("the token could not be generated") 
            });
        }else
        {
            throw Error("invalid password");
        }

        return new AuthenticateUserResponse(token, "Bearer");
    }


}