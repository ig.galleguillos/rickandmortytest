
import ICharacterService from "./ICharacterService";
import Character from "../../../domain/entities/Character";
import fetch from "node-fetch";
import { injectable } from "inversify";

@injectable()
export default class CharacterService implements ICharacterService {
    
    private apiUrl : string = "https://rickandmortyapi.com/api/character";

    async GetRickAndMortyCharacterInformation(): Promise<Character[]> {
        let characters : Character[] = [];

        let result = await fetch(this.apiUrl).then((data) =>{
            return data.json();
        });

        characters = result.results.map((data:any) =>{
            return new Character(data.name, data.status, data.species, data.gender, data.image);
        });
        
        return characters;
    }
}