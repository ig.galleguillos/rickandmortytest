import Character from "../../../domain/entities/Character";


export default interface ICharacterService {

    GetRickAndMortyCharacterInformation() : Promise<Character[]>;
}