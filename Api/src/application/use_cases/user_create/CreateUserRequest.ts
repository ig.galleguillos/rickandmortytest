
export default class CreateUserRequest {
    
    constructor(
        public username: string,
        public password: string
    ){}
}