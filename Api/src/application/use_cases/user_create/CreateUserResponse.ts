
export default class CreateUserResponse{

    constructor(
        public id: string, 
        public username: string
    ){}
}