import ICreateUser from "./ICreateUser";

import CreateUserRequest from "./CreateUserRequest";
import CreateUserResponse from "./CreateUserResponse";
import IHashing from "../../../domain/interfaces/IHashing";
import User from "../../../domain/entities/User";
import IUserRepository from "../../../domain/interfaces/IUserRepository";
import { injectable, inject } from "inversify";
import { TYPES } from "../../../infrastructure/configuration/Types";

@injectable()
export default class CreateUser implements ICreateUser {

    private hashingPassword: IHashing;
    private userRepository: IUserRepository;

    constructor(@inject(TYPES.IHashing) hashingPassword : IHashing, 
        @inject(TYPES.IUserRepository) userRepository: IUserRepository)
    {
        this.hashingPassword = hashingPassword;
        this.userRepository = userRepository;
    }

    async CreateUser(createUserRequest: CreateUserRequest): Promise<CreateUserResponse> {
        
        const userDomain = new User("", createUserRequest.username, createUserRequest.password);
        userDomain.password = await this.hashingPassword.HashPassword(userDomain.password);

        const userResult = await this.userRepository.AddUser(userDomain);

        return new CreateUserResponse(userResult.id, userResult.username);
    }


}