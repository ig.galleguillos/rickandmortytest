import CreateUserRequest from "./CreateUserRequest";
import CreateUserResponse from "./CreateUserResponse";

export default interface ICreateUser{

    CreateUser(createUserRequest : CreateUserRequest) : Promise<CreateUserResponse>;
}