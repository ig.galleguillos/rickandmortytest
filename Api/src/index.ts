
import "reflect-metadata";
import * as http from "http";
import bodyParser from "koa-bodyparser";
import { Container } from "inversify";
import cors from "@koa/cors";
import IUserRepository from "./domain/interfaces/IUserRepository";
import { TYPES } from "./infrastructure/configuration/Types";
import UserRepository from "./infrastructure/repositories/UserRepository";
import HashingService from "./infrastructure/hashing/HashingService";
import IHashing from "./domain/interfaces/IHashing";
import ICreateUser from "./application/use_cases/user_create/ICreateUser";
import CreateUser from "./application/use_cases/user_create/CreateUser";
import { interfaces, InversifyKoaServer, TYPE } from "inversify-koa-utils";
import { UserController } from "./infrastructure/controllers/UserController";
import IJwtTokenManager from "./domain/interfaces/IJwtTokenManager";
import JwtTokenManager from "./infrastructure/jwt/JwtTokenManager";
import IAuthenticateUser from "./application/use_cases/authenticate_user/IAuthenticateUser";
import AuthenticateUser from "./application/use_cases/authenticate_user/AuthenticateUser";
import ICharacterService from "./application/use_cases/characters/ICharacterService";
import CharacterService from "./application/use_cases/characters/CharacterService";
import { CharacterController } from "./infrastructure/controllers/CharacterController";


const container = new Container();

container.bind<IUserRepository>(TYPES.IUserRepository).to(UserRepository);
container.bind<IHashing>(TYPES.IHashing).to(HashingService);
container.bind<ICreateUser>(TYPES.ICreateUser).to(CreateUser);
container.bind<IAuthenticateUser>(TYPES.IAuthenticateUser).to(AuthenticateUser);
container.bind<IJwtTokenManager>(TYPES.IJwtTokenManager).to(JwtTokenManager);
container.bind<ICharacterService>(TYPES.ICharacterService).to(CharacterService);
container.bind<interfaces.Controller>(TYPE.Controller).to(UserController).whenTargetNamed("UserController");
container.bind<interfaces.Controller>(TYPE.Controller).to(CharacterController).whenTargetNamed("CharacterController");

const server = new InversifyKoaServer(container);

server.setConfig((app) =>{
    app.use(bodyParser());
    app.use(cors());
})

const app = server.build();

http.createServer(app.callback()).listen(3000, () =>{
    console.log("application listen on port 3000");
});