
export default interface IHashing {

    HashPassword(password: string) : Promise<string>;
    CompareHashedPassword(passwordFromRequest: string, passwordFromDb: string) : Promise<boolean>;
}