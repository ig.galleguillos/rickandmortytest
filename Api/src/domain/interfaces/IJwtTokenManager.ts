import User from "../entities/User";


export default interface IJwtTokenManager {

    GenerateToken(user : User) : Promise<string>;
}