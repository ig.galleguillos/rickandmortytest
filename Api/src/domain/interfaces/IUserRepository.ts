import User from "../entities/User";

export default interface IUserRepository {

    AddUser(user: User): Promise<User>;
    GetUser(username: string): Promise<User>;
}