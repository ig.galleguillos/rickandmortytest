

export default class Character {

    constructor(
        public name:string,
        public status:string,
        public species:string,
        public gender:string,
        public imageUrl:string
    ){}
}