
module.exports = {
    "roots": [
      "test",
      "src"
    ],
    "testMatch": [
      "**/__tests__/**/*.+(ts|tsx|js)",
      "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    "transform": {
      "^.+\\.(ts|tsx)$": "ts-jest"
    },
    "collectCoverage": true,
    "collectCoverageFrom": ['src/**/*.{ts,js,jsx}']
  }